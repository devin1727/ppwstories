from django.urls import path,include
from .views import *
urlpatterns = [
    path('', make_form,name = 'form'),
    path('addSubscribe/', addSub, name="addSubscribe"),
	path('validasi/', validasi, name="validasi"),
]