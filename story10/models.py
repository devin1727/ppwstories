from django.db import models
from django import forms

class User(models.Model):
	email = models.EmailField()
	username = models.CharField(max_length = 200)
	password = models.CharField(max_length = 200)