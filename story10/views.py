from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from story10.models import User
from .forms import EmailForm
from .forms import UsernameForm
from .forms import PasswordForm
import json
from django.core import serializers

response = {}

def make_form(request):
	email_form = EmailForm(request.POST or None)
	user_form = UsernameForm(request.POST or None)
	pass_form = PasswordForm(request.POST or None)
	return render(request, "regis.html", {'email_form':email_form,'user_form':user_form,'pass_form':pass_form})

@csrf_exempt	
def addSub(request):
	if (request.method == 'POST'):
		enctype = "multipart/form-data"
		nama = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']

		subscriber = User(nama = nama, email = email, password = password)
		subscriber.save()
		
		data = getObject(subscriber)
		return HttpResponse(data)
	else:
		sub = User.objects.all().values()
		data = list(sub)
		return JsonResponse(data, safe = False)

@csrf_exempt
def validasi(request):
	enctype = "multipart/form-data"
	email = request.POST.get('email')
	data = {
		'is_taken':User.objects.filter(email=email).exists()
		}
	return JsonResponse(data)

def getObject(obj):
	data = serializers.serialize('json', [obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data
