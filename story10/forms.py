from django import forms

class EmailForm(forms.Form):
	attrs={
		'class' : 'input100 focus-input100',
		'type' : 'email',
		'name' : 'email',
		'id' : 'text2',
		'placeholder' : 'Email'
	}
	email = forms.EmailField(widget=forms.EmailInput(attrs=attrs))

class UsernameForm(forms.Form):
	attrs={
		'class' : 'input100 focus-input100',
		'type' : 'text',
		'name' : 'username',
		'id' : 'text1',
		'placeholder' : 'Username',
	}
	username = forms.CharField(widget=forms.TextInput(attrs=attrs), max_length=50)

class PasswordForm(forms.Form):
	attrs={
		'class' : 'input100 focus-input100',
		'type' : 'text',
		'name' : 'pass',
		'id' : 'text3',
		'placeholder' : 'Password'
	}
	password = forms.CharField(widget=forms.PasswordInput(attrs=attrs), min_length=8)