from django.test import TestCase,Client
from django.urls import resolve
from story10.views import *
from story10.models import User

class SubscribeFormTest(TestCase):
	
	def test_view_get_return_301(self):
		"""
		Test request get return 200
		"""
		response = Client().get('/story10')
		self.assertEqual(response.status_code, 301)

	def test_views_using_landing_func(self):
		"""
		Test if a reverse using correct views function
		"""
		found = resolve('/story10/')
		self.assertEqual(found.func, make_form)

	def test_model_can_create_new_subscriber(self):
		new_subscriber = User.objects.create(username ="devin", email="test@bla.com",password="password")
		counting_all_available_donation = User.objects.all().count()
		self.assertEqual(counting_all_available_donation, 1)