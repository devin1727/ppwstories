$(document).ready(function(){
	$.ajax({
		type:'GET',
		dataType :'json',
		url: 'https://www.googleapis.com/books/v1/volumes?q=quilting',
		crossDomain: true,
		success: function(data){
			var html_string = "";
			var unique_id;
			$.each(data.items,function(i,book){
				unique_id = ""+book.volumeInfo.industryIdentifiers[1].identifier;
				if (unique_id.startsWith("0")){
					unique_id = unique_id.slice(1,unique_id.length);
				}
				if (unique_id.endsWith("X")){
					unique_id = unique_id.slice(0,unique_id.length -1);
				}
				var star_url = "https://i.imgur.com/7CeVmZu.png";
				html_string += "<tr>" + "<td>" +
				"<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
	             "<td id='title'>"+book.volumeInfo.title+"</td>"+
	             "<td>"+book.volumeInfo.publisher+"</td>"+
	             " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"max-width:20px;max-height:20px;\"></td>"+
	             "</tr>";
			})
			$("#start").append(html_string)
		}
	})
})

var favorites = 0;

function star(the_id){
	var star_url_biru = "https://i.imgur.com/7CeVmZu.png";
	var star_url_kuning = "https://i.imgur.com/SPTSn8Y.png";

	if(document.getElementById(the_id).src==star_url_biru){
        //console.log('success',the_id);
        favorites+=1;
        document.getElementById(the_id).src=star_url_kuning;
    }
    else{
       // console.log('succes',document.getElementById(the_id).src);
        if(favorites>0){
            favorites-=1;
        }
        else{
            favorites=0;
        }
        document.getElementById(the_id).src=star_url_biru;
    }
    document.getElementById("fav").innerHTML = "Favorites: "+favorites;
}



