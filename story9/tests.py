from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import login, index, get_Json
from django.apps import apps

class Lab11Test(TestCase):
    
    def test_story11_login_func(self):
        found = resolve('/story9/login')
        self.assertEqual(found.func, login)

    def test_story11_index_func(self):
        found = resolve('/story9/books')
        self.assertEqual(found.func, index)    
    
    def test_template_story11(self):
        response = Client().get('/story9/login')
        self.assertTemplateUsed(response, 'login.html')

    def test_template_story11(self):
        response = Client().get('/story9/books')
        self.assertTemplateUsed(response, 'index.html')