from django.contrib import admin
from django.urls import path,include
from .views import index
from .views import get_Json
from .views import login
from .views import signout

urlpatterns = [
    path('login', login,name = 'login'),
    path('books',index, name = 'booklist'),
    path('logout', signout,name='signout'),
    path('get_json',get_Json,name='getjson'),
]