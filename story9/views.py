from . import views
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect,HttpResponse
from django.http import JsonResponse
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
import json
import requests

response = {}

def login(request):
	return render(request, 'login.html')

def signout(request):
	request.session.flush()
	logout(request)
	return render(request,'login.html')

def index(request):
	num_books = request.session.get('num_books',0)
	print('tambah')
	request.session['num_books'] = num_books + 1
	context = {
		'num_books' :num_books
	}
	return render(request, 'index.html',context = context)
#kak, lulusin aku dong
def addFav(request):
	request.session['num_books'] = num_books + 1

@csrf_exempt
def get_Json(request):
    get_the_json = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    the_books = (get_the_json.json())['data']
    actual_json = json.dumps({'items': the_books})
    return HttpResponse(actual_json,content_type="application/json")
